## **Tia Burton**
**Generative AI Solutions Developer**

[LinkedIn](https://www.linkedin.com/in/tiaburton)


I accelerate digital transformation with Google Cloud's Generative AI solutions. I work with customers and cross-functional teams to drive revenue growth and product adoption. I'm passionate about building and delivering innovative solutions, with special emphasis on Software & Technology, Logistics and Manufacturing industries.

## Skills and Technologies

* Programming Languages: Python, Spark, HTML, CSS, JS, Go
* Generative AI
* Cloud Computing
* Digital Transformation

## Badges and Visuals

* [Credly Badges](https://www.credly.com/users/googler-tia-burton)


## Community Contributions

### Open Source
* N/A for now